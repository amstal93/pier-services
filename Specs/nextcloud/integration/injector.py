from utils import merge_dicts 
from passwords import passwordManager
from exit_codes import ExitCode
from config import FacilitiesWrapper
from os import makedirs
import logging

logger = logging.getLogger("log")

class Injector:
    def handle_spec(self, facilities: FacilitiesWrapper, service_name, spec, own_config = None):
        logger.debug("Injecting the nextcloud link for %s", service_name)
        logger.increaseIndent()
        self.inject_base_configuration(facilities, service_name)
        handlers = {
            "expose_volumes": self.expose_volume,
        }
        if isinstance(spec, dict):
            for (key, value) in spec.items():
                handle = handlers.get(key, lambda _,__,___ : logger.warning('Unsupported nextcloud injection key: %s', key))
                handle(facilities, service_name, value)
        logger.decreaseIndent()
        

    def inject_base_configuration (self, facilities: FacilitiesWrapper, service_name):
        nc_compose = {
            "services":{
                service_name:{
                    "depends_on": ["nextcloud"]
                }
            }
        }
        facilities.inject_config(nc_compose)

    

    def expose_volume(self, facilities: FacilitiesWrapper, service_name, volumes):
        if isinstance(volumes, list):
            for volume in volumes:
                nc_compose = {
                    "services":{
                        "nextcloud":{
                            "volumes": [volume + ":" + "/var/www/html/data/admin/files/services-volumes/" + volume]
                        }
                    }
                }
                facilities.inject_config(nc_compose)

        else:
            logger.critical("Nextcloud injection key 'expose_volumes' should be a list.")
            exit(ExitCode.BAD_SPEC_FILE_FORMAT)
        