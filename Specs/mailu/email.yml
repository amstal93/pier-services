version: '3.5'

services:

    # External dependencies
    mailu-redis:
        image: redis:alpine
        container_name: mailu-redis
        restart: always
        volumes:
            - mailu_redis:/data
        networks:
            mailu-internal:
                aliases:
                    - redis

    # Core services
    mailu:
        image: leolivier/mailu_nginx:rpi-armv7-1.8-release
        restart: always
        env_file: mailu/mailu.env
        environment:
            HOSTNAMES: "${DOMAIN}"
            DOMAIN: "${MX_DOMAIN}"
            INITIAL_ADMIN_DOMAIN: "${MX_DOMAIN}"
            INITIAL_ADMIN_PW: "${INITIAL_ADMIN_PW}"
        logging:
            driver: json-file
        ports:
            - "127.0.0.1:25:25"
            - "127.0.0.1:465:465"
            - "127.0.0.1:587:587"
            - "127.0.0.1:110:110"
            - "127.0.0.1:995:995"
            - "127.0.0.1:143:143"
            - "127.0.0.1:993:993"
        volumes:
            - mailu_certs:/certs
            - mailu_overrides_nginx:/overrides
        networks:
            mailu-internal:
                aliases:
                    - front
    
    mailu-resolver:
        image: leolivier/mailu_unbound:rpi-armv7-1.8-release
        env_file: mailu/mailu.env
        container_name: mailu-resolver
        restart: always
        networks:
            mailu-internal:
                ipv4_address: 192.168.203.254
                aliases:
                    - resolver

    mailu-admin:
        image: leolivier/mailu_admin:rpi-armv7-1.8-release
        container_name: mailu-admin
        restart: always
        env_file: mailu/mailu.env
        environment:
            DB_PW: "${MAILU_DB_PASS}"
        volumes:
            - mailu_data:/data
            - mailu_dkim:/dkim
        depends_on:
            - mailu-redis
        networks:
            mailu-internal:
                aliases:
                    - admin

    mailu-imap:
        image: leolivier/mailu_dovecot:rpi-armv7-1.8-release
        container_name: mailu-imap
        restart: always
        env_file: mailu/mailu.env
        environment:
            HOSTNAMES: "${DOMAIN}"
            DOMAIN: "${MX_DOMAIN}"
            INITIAL_ADMIN_DOMAIN: "${MX_DOMAIN}"
        volumes:
            - mailu_mail:/mail
            - mailu_overrides:/overrides
            - mailu_overrides_nginx:/overrides/nginx
            - mailu_overrides_rspamd:/overrides/rspamd
        depends_on:
            - mailu
        networks:
            mailu-internal:
                aliases:
                    - imap

    mailu-smtp:
        image: leolivier/mailu_postfix:rpi-armv7-1.8-release
        container_name: mailu-smtp
        restart: always
        env_file: mailu/mailu.env
        environment:
            HOSTNAMES: "${DOMAIN}"
            DOMAIN: "${MX_DOMAIN}"
            INITIAL_ADMIN_DOMAIN: "${MX_DOMAIN}"
        volumes:
            - mailu_mailqueue:/queue
            - mailu_overrides:/overrides
            - mailu_overrides_nginx:/overrides/nginx
            - mailu_overrides_rspamd:/overrides/rspamd
        depends_on:
            - mailu
            - mailu-resolver
        dns:
            - 192.168.203.254
        networks:
            mailu-internal:
                aliases:
                    - smtp

    mailu-antispam:
        image: leolivier/mailu_rspamd:rpi-armv7-1.8-release
        container_name: mailu-antispam
        restart: always
        env_file: mailu/mailu.env
        volumes:
            - mailu_filter:/var/lib/rspamd
            - mailu_dkim:/dkim
            - mailu_overrides_rspamd:/etc/rspamd/override.d
        depends_on:
            - mailu
            - mailu-resolver
        dns:
            - 192.168.203.254
        networks:
            mailu-internal:
                aliases:
                    - antispam

    # Optional services

    mailu-webdav:
        image: leolivier/mailu_radicale:rpi-armv7-1.8-release
        container_name: mailu-webdav
        restart: always
        env_file: mailu/mailu.env
        volumes:
            - mailu_dav:/data
        networks:
            mailu-internal:
                aliases:
                    - webdav


    # Webmail
    mailu-webmail:
        image: leolivier/mailu_roundcube:rpi-armv7-1.8-release
        container_name: mailu-webmail
        restart: always
        env_file: mailu/mailu.env
        environment:
            ROUNDCUBE_DB_PW: "${MAILU_DB_PASS}"
            HOSTNAMES: "${DOMAIN}"
            DOMAIN: "${MX_DOMAIN}"
            INITIAL_ADMIN_DOMAIN: "${MX_DOMAIN}"
        volumes:
            - mailu_webmail:/data
        depends_on:
            - mailu-imap
        networks:
            mailu-internal:
                aliases:
                    - webmail
            db-network: {}

networks:
    mailu-internal:
        name: mailu-internal
        driver: bridge
        ipam:
            driver: default
            config:
                - subnet: 192.168.203.0/24
    db-network:
        external: true
        name: db-network
