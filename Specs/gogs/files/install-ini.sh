#!/bin/sh

mkdir -p "/data/gogs/conf/" "/data/git/.ssh/" "/data/gogs/data/" "/data/gogs/log/"
cp "/inifile" "/data/gogs/conf/app.ini"
chown -R ${PUID}:${PGID} "/data"
sed -i "s|REPLACE_WITH_DEVICE_DNS|${DOMAIN}|" "/data/gogs/conf/app.ini"
sed -i "s|REPLACE_WITH_DB_PASSWORD|${DB_PASS}|" "/data/gogs/conf/app.ini"
sed -i "s|REPLACE_WITH_SECRET|${SECRET_KEY}|" "/data/gogs/conf/app.ini"

/app/gogs/docker/start.sh "$@"