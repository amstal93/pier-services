from utils import merge_dicts 
from passwords import passwordManager
from config import FacilitiesWrapper
from os import makedirs
import logging

logger = logging.getLogger("log")

class Injector:
    def handle_spec(self, facilities: FacilitiesWrapper, service_name, spec, own_config = None):
        logger.debug("Injecting the database for %s", service_name)
        logger.increaseIndent()
        self.inject_base_configuration(facilities, service_name)
        handlers = {
            "create_own_db": self.generate_own_db,
        }
        if isinstance(spec, dict):
            for (key, value) in spec.items():
                handle = handlers.get(key, lambda _,__,___ : logger.warning('Unsupported db injection key: %s', key))
                handle(facilities, service_name, value)
        logger.decreaseIndent()
        

    def inject_base_configuration (self, facilities: FacilitiesWrapper, service_name):
        db_compose = {
            "services":{
                service_name:{
                    "networks": {"db-network":{}}
                }
            },
            "networks": { 
                "db-network" : {
                    "name": "db-network",
                    "external": True
                }
            }
        }
        facilities.inject_config(db_compose)

    

    def generate_own_db(self, facilities: FacilitiesWrapper, service_name, value):
        if not value:
            return
        pass_id = service_name.upper() + "_DB_PASS"
        logger.debug("Creating database %s for user %s", service_name, service_name)
        password = facilities.pass_manager.get_secret_with_id(pass_id)
        create_usr_str = "DO $$\n"
        create_usr_str += "BEGIN\n"
        create_usr_str += "PERFORM dblink_exec('', 'CREATE DATABASE " + service_name + "');\n"
        create_usr_str += "EXCEPTION WHEN duplicate_database THEN RAISE NOTICE '%, skipping', SQLERRM USING ERRCODE = SQLSTATE;\n"
        create_usr_str += "END\n"
        create_usr_str += "$$;\n"
        create_usr_str += "DO $$\n"
        create_usr_str += "BEGIN\n"
        create_usr_str += "  CREATE ROLE " + service_name + " LOGIN PASSWORD '" + password + "';\n"
        create_usr_str += "  GRANT ALL PRIVILEGES ON DATABASE " + service_name + " TO " + service_name + ";\n"
        create_usr_str += "  EXCEPTION WHEN DUPLICATE_OBJECT THEN\n"
        create_usr_str += "  RAISE NOTICE 'not creating user " + service_name + " -- it already exists';\n"
        create_usr_str += "END\n"
        create_usr_str += "$$;\n"

        dir_name = facilities.srv_config.dest_dir + "/db/"
        makedirs(dir_name, exist_ok=True)
        file = open(dir_name + "/create-databases.sql", "a")
        file.write(create_usr_str)
        file.close()



    
        