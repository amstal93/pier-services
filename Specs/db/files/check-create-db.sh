#!/usr/bin/env bash

# This script checks if new databases have been added in /always-initdb.d and creates them if needed
# Assumes the postgres server is already running
# Based on a part of the custom-entrypoint.sh code

set -Eeo pipefail

source "$(which docker-entrypoint.sh)"

echo
echo 'PostgreSQL Database directory appears to contain a database; Only running /always-initdb.d/* if changed'
echo

newchecksum=$(tar -c /always-initdb.d | sha1sum | awk '{print $1}')
if [[ -e /initchecksum ]]; then
    oldchecksum=$(cat /initchecksum)
else
    echo 'old checksum file didnt exist'
fi
echo "old:$oldchecksum"
echo "new:$newchecksum"
if [[ "$newchecksum" != "$oldchecksum" ]]; then
    echo '/always-initdb.d has changed, rerunning contents'
    echo $newchecksum > /initchecksum
    docker_process_init_files /always-initdb.d/*
else
    echo 'No changes in /always-initdb.d'
fi
