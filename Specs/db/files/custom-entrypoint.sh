#!/usr/bin/env bash
set -Eeo pipefail

source "$(which docker-entrypoint.sh)"
# THE FOLLOWING IS COPIED FROM THE OFFICIAL docker-entrypoint.sh
# CHANGES MARKED BY "#CHANGE"


docker_setup_env
# setup data directories and permissions (when run as root)
docker_create_db_directories
if [ "$(id -u)" = '0' ]; then
    # then restart script as postgres user
    exec su-exec postgres "$BASH_SOURCE" "$@"
fi

# only run initialization on an empty data directory
if [ -z "$DATABASE_ALREADY_EXISTS" ]; then
    docker_verify_minimum_env

    # check dir permissions to reduce likelihood of half-initialized database
    ls /docker-entrypoint-initdb.d/ > /dev/null

    docker_init_database_dir
    pg_setup_hba_conf

    # PGPASSWORD is required for psql when authentication is required for 'local' connections via pg_hba.conf and is otherwise harmless
    # e.g. when '--auth=md5' or '--auth-local=md5' is used in POSTGRES_INITDB_ARGS
    export PGPASSWORD="${PGPASSWORD:-$POSTGRES_PASSWORD}"
    docker_temp_server_start "$@"

    docker_setup_db
    docker_process_init_files /docker-entrypoint-initdb.d/*
    #CHANGE BEGIN
    docker_process_init_files /always-initdb.d/*
    checksum=$(tar -c /always-initdb.d | sha1sum | awk '{print $1}')
    echo $checksum > /initchecksum
    echo "initchecksum: $checksum"
    #CHANGE END
    docker_temp_server_stop
    unset PGPASSWORD

    echo
    echo 'PostgreSQL init process complete; ready for start up.'
    echo
else
    #CHANGE BEGIN
    echo
    echo 'PostgreSQL Database directory appears to contain a database; Only running /always-initdb.d/* if changed'
    echo

    newchecksum=$(tar -c /always-initdb.d | sha1sum | awk '{print $1}')
    if [[ -e /initchecksum ]]; then
	oldchecksum=$(cat /initchecksum)
    else
        echo 'old checksum file didnt exist'
    fi
    echo "old:$oldchecksum"
    echo "new:$newchecksum"
    if [[ "$newchecksum" != "$oldchecksum" ]]; then
        echo '/always-initdb.d has changed, rerunning contents'
        echo $newchecksum > /initchecksum
        docker_temp_server_start "$@"
        docker_process_init_files /always-initdb.d/*
        docker_temp_server_stop
    else
        echo 'No changes in /always-initdb.d'
    fi
    #CHANGE END
fi

# Call original entrypoint
docker-entrypoint.sh "$@"