import os
import logging

logger = logging.getLogger("log")

def clone_repo_in_folder(repo_url: str, dest_folder: str, branch, commit):
    os.makedirs(dest_folder, exist_ok= True)
    # find default folder name
    last_slash_index = repo_url.rfind('/')
    end_of_path = len(repo_url)
    if repo_url.endswith(".git"):
        end_of_path -= len(".git")
    name = repo_url[last_slash_index + 1 : end_of_path]
    if os.path.exists(dest_folder + "/" +name):
        logger.debug("Repository %s already exists, pulling", name)
        # TODO: We do not checkthat it is a repo or that the remote is the right one
        command = "cd '"+ dest_folder +"' && git pull"
    else:
        # TODO: We do not check the results
        logger.debug("Repository %s did not already exist, cloning", name)
        command = "cd '"+ dest_folder +"' && git clone"
        if isinstance(branch, str) and branch != "":
            command += " --branch " + branch
        command += " " + repo_url
        if isinstance(commit, str) and commit != "":
            command += "> /dev/null && cd " + name + " && git checkout " + commit
        command += "> /dev/null && echo 'cloned "+ name+"'"
        os.system(command)
