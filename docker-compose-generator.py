#!/usr/bin/env python3
import argparse
import logging
from indent_logger import IndentLogger
logging.setLoggerClass(IndentLogger)

from yaml import load
from os import path
from sys import stdout
from distutils.dir_util import copy_tree
from glob import glob
from utils import merge_dicts, install_file_with_content, read_config
from config import FacilitiesWrapper
from exit_codes import ExitCode
import git_repo_utils

logger = logging.getLogger("log")
critical_facilities = None

def inject_universal_config(facilities: FacilitiesWrapper, service_name):
    compose_dict = {}
    compose_dict["version"] = "3.5"
    service_config = {}
    service_config["container_name"] = service_name
    service_config["restart"] = "unless-stopped"
    compose_dict["services"] = {
        service_name : service_config
    }
    facilities.inject_config(compose_dict)


def inject_volume_config(facilities: FacilitiesWrapper, service_name, mount_points):
    if not isinstance(mount_points, list):
        logger.critical("volume_on should be an array of mount point dicts")
        exit(ExitCode.BAD_SPEC_FILE_FORMAT)
    else:
        path = ["services", service_name, "volumes"]
        existing_mounts = facilities.get_current_compose_value(path)
        if not isinstance(existing_mounts, list):
            existing_mounts = []
        offset = len(existing_mounts)
        for num, volume_spec in enumerate(mount_points):
            if not isinstance(volume_spec, dict):
                logger.critical("each volume specified in volume_on should be a dict of mount_point and optionally volume_name")
                exit(ExitCode.BAD_SPEC_FILE_FORMAT)
            docker_compose = {}
            # define the volume name
            volume_name = ""
            if "volume_name" in volume_spec:
                volume_name = volume_spec["volume_name"]
            else:
                volume_name = service_name + "_autogen_vol_" + str(num + offset)

            is_bind_mount = False
            if "mount_point" in volume_spec:
                mount_point = volume_spec["mount_point"]
                # check if it is a bind mount
                service_config = facilities.srv_config.config["services"][service_name]
                if "volumes" in service_config and mount_point in service_config["volumes"]:
                    is_bind_mount = True
                    volume_name = service_config["volumes"][mount_point]["bind_location"]
                # mount volume in service
                docker_compose["services"] = {
                    service_name: {
                        "volumes": [volume_name + ":" + mount_point]
                    }
                }
            else:
                logger.info("volume %s specified in volume_on does not contain a mount_point key, creating an independent volume.", volume_name)
            # create volume only if it is not a bind mount
            if not is_bind_mount:
                docker_compose["volumes"] = {
                    volume_name: {
                        "name": volume_name,
                        "labels": {
                            "pier": "",
                            "pier.fromspec": service_name
                        }
                    }
                }

            facilities.inject_config(docker_compose)

            if "backup" in volume_spec:
                if is_bind_mount:
                    logger.critical("backup is not supported for bind mounts")
                    exit(ExitCode.FORBIDDEN_MOUNT_CONFIG)
                if volume_spec["backup"] == False:
                    label = {
                        "volumes":{
                            volume_name:{
                                "labels": {
                                    "pier.nobackup"
                                }
                            }
                        }
                    }
                    facilities.inject_config(label)
                elif volume_spec["backup"] != True:
                    logger.info("backup property in volume_on should either be true or false, assuming default: true")


def inject_dockerfile(facilities: FacilitiesWrapper, service_name, dockerfile_url):
    if dockerfile_url.startswith("dockerhub://"):
        # strip the url method from the name
        compose = {
            "services":{
                service_name:{
                    "image": dockerfile_url[len("dockerhub://"):]
                }
            }
        }
    elif dockerfile_url.startswith("file://"):
        # strip the url method from the name
        docker_file = dockerfile_url[len("file://"):]
        compose = {
            "services":{
                service_name:{
                    "build": {
                        "context": "./" + service_name,
                        "dockerfile": docker_file
                    }
                }
            }
        }
    else:
        logger.critical("Unsupported dockerfile: %s", dockerfile_url)
        exit(ExitCode.BAD_DOCKERFILE_REFERENCE)
    facilities.inject_config(compose)


def inject_optional(facilities: FacilitiesWrapper, service_name, optionals):
    for optional in optionals:
        if isinstance(optional, dict) and len(optional) == 1:
            service_to_inject = list(optional.keys())[0]
            if service_to_inject in facilities.injectors:
                facilities.injectors[service_to_inject].handle_spec(facilities, service_name, optional[service_to_inject])
        else:
            logger.critical("Invalid optionals spec for %s: %s", service_name, optional)
            exit(ExitCode.BAD_SPEC_FILE_FORMAT)


def inject_requirements(facilities: FacilitiesWrapper, service_name, requirements):
    for requirement in requirements:
        if isinstance(requirement, dict) and len(requirement) == 1:
            service_to_inject = list(requirement.keys())[0]
            if service_to_inject in facilities.injectors:
                facilities.injectors[service_to_inject].handle_spec(facilities, service_name, requirement[service_to_inject])
            if service_to_inject in critical_facilities.injectors:
                critical_facilities.injectors[service_to_inject].handle_spec( \
                    facilities, service_name, requirement[service_to_inject], \
                    own_config=critical_facilities.srv_config.get_config_for(service_to_inject))
            elif facilities.srv_config.will_service_be_installed(service_to_inject):
                logger.warning("Requirement on service that did not specify an injector: %s", service_to_inject)
                compose = {
                    "services":{
                        service_name:{
                            "depends_on": [service_to_inject]
                        }
                    }
                }
                facilities.inject_config(compose)
            else:
                logger.critical("Requirement on service %s by %s, but this service is not installed.", service_to_inject, service_name)
                exit(ExitCode.SERVICE_NOT_FOUND)
        else:
            logger.critical("Invalid requirements spec: %s", requirement)
            exit(ExitCode.BAD_SPEC_FILE_FORMAT)


def inject_passwords(facilities: FacilitiesWrapper, service_name, pass_requirements):
    for req in pass_requirements:
        if "id" in req:
            # Ensure that the password exists and is documented in the .env file
            if "expose_as" in req:
                expose_as = req["expose_as"]
            else:
                expose_as = req["id"]
            
            if "hashed" in req:
                hashtype = req["hashed"]
            else:
                hashtype = "no"
            
            if "type" in req:
                if req["type"] == "key":
                    generate_key = True
                elif req["type"] == "password":
                    generate_key = False
                else:
                    logger.critical("Unsupported password type %s", req["type"])
                    exit(ExitCode.UNSUPPORTED_PASSWORD_TYPE)
            else:
                generate_key = False

            facilities.pass_manager.expose_secret(req["id"], hashtype, generate_key)
            # expose it as an environment variable
            complement_compose = {
                    "services":{
                        service_name:{
                            "environment": {expose_as: '${' + req["id"] + '}'}
                        }
                    }
                }
            facilities.inject_config(complement_compose)

        else:
            logger.critical("Invalid password spec: %s", req)
            exit(ExitCode.PASSWORD_ID_NOT_SPECIFIED)

def source_repo_files(facilities: FacilitiesWrapper, service_name: str, config: dict):
    dest_dir = facilities.srv_config.dest_dir + '/' + service_name
    branch = None
    if "branch" in config:
        branch = config["branch"]
    commit = None
    if "commit" in config:
        commit = config["commit"]
    git_repo_utils.clone_repo_in_folder(config["url"], dest_dir, branch, commit)


def handle_spec_top_level_rule(facilities: FacilitiesWrapper, service_name, spec):
    for rule in spec:
        if "if" in rule and "do" in rule:
            condition = rule["if"]
            try:
                ok = facilities.srv_config.check_fullfillment(condition)
            except KeyError:  # service not found in facilities, see if in critical
                ok = critical_facilities.srv_config.check_fullfillment(condition)
            
            if ok:
                logger.debug("Condition %s is fulfilled, handling do block", condition)
                do = rule["do"]
                logger.increaseIndent()
                handle_spec_top_level_rule(facilities, service_name, do)
                logger.decreaseIndent()
            elif "else" in rule:
                logger.debug("Condition %s is not fulfilled, handling else block", condition)
                logger.increaseIndent()
                handle_spec_top_level_rule(facilities, service_name, rule["else"])
                logger.decreaseIndent()
        if "auto-generation" in rule:
            logger.debug("Entering auto generation block")
            logger.increaseIndent()
            handlers = {
                "dockerfile": inject_dockerfile,
                "optional": inject_optional,
                "requires": inject_requirements,
                "volume_on": inject_volume_config,
                "passwords": inject_passwords,
                "files_from_repository": source_repo_files
            }
            auto_generation_dict = rule["auto-generation"]
            for (key, value) in auto_generation_dict.items():
                logger.debug("Calling handler for: %s", key)
                handle = handlers.get(key, lambda _,__,___ : logger.warning('Unsupported auto-generation key: %s', key))
                logger.increaseIndent()
                handle(facilities, service_name, value)
                logger.decreaseIndent()
            logger.decreaseIndent()
        if "docker_compose_native" in rule:
            logger.debug("Parsing native docker-compose rules")
            overriding = rule["docker_compose_native"]
            facilities.inject_config(overriding)
        if "docker_compose_file" in rule:
            file_name = rule["docker_compose_file"]
            if file_name.startswith("file://"):
                # strip the url method from the name
                docker_file = facilities.srv_config.specs_dir + '/' + service_name + '/' + file_name[len("file://"):]
                logger.debug("Reading docker compose file: %s", docker_file)
                file = open(docker_file, "r")
                overriding = load(file)
                file.close()
                facilities.inject_config(overriding)
            else:
                logger.critical("Unsupported compose file format, aborting")
                exit(ExitCode.BAD_COMPOSEFILE_REFERENCE)
        if "spec_file" in rule:
            file_name = rule["spec_file"]
            if file_name.startswith("file://"):
                spec_file = facilities.srv_config.specs_dir + '/' + service_name + '/' + file_name[len("file://"):]
                logger.debug("Inserting spec file: %s", spec_file)
                file = open(spec_file, "r")
                contents = load(file)
                file.close()
                handle_spec_top_level_rule(facilities, service_name, contents)
            else:
                logger.critical("Unsupported specfile reference format, aborting")
                exit(ExitCode.BAD_SPECFILE_REFERENCE)


def install_service(facilities: FacilitiesWrapper, service_name):
    logger.info("[%s]: Installing...", service_name)
    logger.increaseIndent()
    inject_universal_config(facilities, service_name)

    # copy files if needed
    specs_dir = facilities.srv_config.specs_dir
    dest_dir = facilities.srv_config.dest_dir
    if path.isdir(specs_dir + '/' + service_name + '/files'):
        directory = dest_dir + '/' + service_name
        logger.debug("Copying recursively %s/%s/files to %s", specs_dir, service_name, directory)
        copy_tree(specs_dir + '/' + service_name + '/files', directory, preserve_mode=1)
    # handle spec file
    spec = read_config(specs_dir + '/' + service_name + '/spec.yml')
    handle_spec_top_level_rule(facilities, service_name, spec)
    logger.decreaseIndent()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-A", "--all", 
                    help="generates all 3 stacks (critical+admin+user). Default if no arg, ignored if another argument given",
                    action="store_true",
                    default=True)
    parser.add_argument("-u", "--user", action="store_true",
                    help="generates user stack")
    parser.add_argument("-a", "--admin", action="store_true",
                    help="generates admin stack")
    parser.add_argument("-c", "--critical", action="store_true",
                    help="generates critical stack")
    args = parser.parse_args()
    if args.user or args.admin or args.critical: args.all = False
    stacks = []
    if args.all or args.critical: stacks.append("critical")
    if args.all or args.admin: stacks.append("admin")
    if args.all or args.user: stacks.append("user")

#    logging.warning("stacks:" + ', '.join(stacks))
#    exit(1)


    formatShort = logging.Formatter("%(levelname)-8s - %(message)s")
    logger.setLevel(logging.DEBUG)
    streamHandler = logging.StreamHandler(stream=stdout)
    streamHandler.setLevel(logging.INFO)
    streamHandler.setFormatter(formatShort)
    formatLong = logging.Formatter("%(asctime)s - %(levelname)-8s - %(message)s", "%Y-%m-%d %H:%M:%S")
    logger.addHandler(streamHandler)
    fileHandler = logging.FileHandler('generation.log',mode='w')
    fileHandler.setLevel(logging.DEBUG)
    fileHandler.setFormatter(formatLong)
    logger.addHandler(fileHandler)

    global critical_facilities
    critical_facilities = FacilitiesWrapper("critical")

    for stack in stacks:
        # to expand blocks, pass ", default_flow_style=False" to dump
        facilities = FacilitiesWrapper(stack)
        facilities.init_env()
        for service_name in facilities.srv_config.get_services_to_install():
            install_service(facilities, service_name)
        facilities.dump_compose_to('docker-compose.yml')

if __name__ == "__main__":
    # execute only if run as a script (useful to import functions or other without side effects)
    main()
