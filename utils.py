from os import environ, makedirs, chmod, path
from yaml import full_load, dump
from exit_codes import ExitCode
import re
import logging

logger = logging.getLogger("log")

# merges two dictionnaries, recursively, authority overrides dest
def merge_dicts(dest, authority):
    for (key, value) in authority.items():
        # if this key refer to a dict in both object, recurse
        if(isinstance(value, dict) and key in dest and isinstance(dest[key], dict)):
            merge_dicts(dest[key], authority[key])
        elif(isinstance(value, list) and key in dest and isinstance(dest[key], list)):
            dest[key].extend(authority[key])
        elif key in dest and ((isinstance(value, list) and isinstance(dest[key], dict)) or (isinstance(value, dict) and isinstance(dest[key], list))):
            logger.critical("Tried to combine a list and dict for %s, aborting", key)
            exit(ExitCode.COMBINING_LIST_AND_DICT)
        else:
            dest[key] = authority[key]

def install_file_with_content(dir_name, file_name, content, mode):
    makedirs(dir_name, exist_ok=True)
    file = open(dir_name + "/" + file_name, "w")
    file.write(content)
    file.close()
    chmod(dir_name + "/" + file_name, mode)

def read_config(path_to_config_file):
    logger.debug("Reading config from %s", path_to_config_file)
    file = open(path_to_config_file, "r")
    yaml_config = full_load(file)
    file.close()
    return yaml_config
